# LocalGovDrupal Elections Reporting

Adds capability to report on Elections

## Before adding to site

Add the following to the sites main composer.json in the `"repositories": [... ]` section.

```
        {
            "type": "package",
            "package": {
                "name": "highcharts/highcharts",
                "version": "8.2.2",
                "type": "drupal-library",
                "extra": {
                    "installer-name": "highcharts"
                },
                "dist": {
                    "url": "https://code.highcharts.com/8.2.2/highcharts.js",
                    "type": "file"
                },
                "require": {
                    "composer/installers": "^1.0 || ^2.0"
                }
            }
        },
        {
            "type": "package",
            "package": {
                "name": "highcharts/more",
                "version": "8.2.2",
                "type": "drupal-library",
                "extra": {
                    "installer-name": "highcharts_more"
                },
                "dist": {
                    "url": "https://code.highcharts.com/8.2.2/highcharts-more.js",
                    "type": "file"
                },
                "require": {
                    "composer/installers": "^1.0 || ^2.0"
                }
            }
        },
        {
            "type": "package",
            "package": {
                "name": "highcharts/exporting",
                "version": "8.2.2",
                "type": "drupal-library",
                "extra": {
                    "installer-name": "highcharts_exporting"
                },
                "dist": {
                    "url": "https://code.highcharts.com/8.2.2/modules/exporting.js",
                    "type": "file"
                },
                "require": {
                    "composer/installers": "^1.0 || ^2.0"
                }
            }
        },
        {
            "type": "package",
            "package": {
                "name": "highcharts/export-data",
                "version": "8.2.2",
                "type": "drupal-library",
                "extra": {
                    "installer-name": "highcharts_export-data"
                },
                "dist": {
                    "url": "https://code.highcharts.com/8.2.2/modules/export-data.js",
                    "type": "file"
                },
                "require": {
                    "composer/installers": "^1.0 || ^2.0"
                }
            }
        },
        {
            "type": "package",
            "package": {
                "name": "highcharts/accessibility",
                "version": "8.2.2",
                "type": "drupal-library",
                "extra": {
                    "installer-name": "highcharts_accessibility"
                },
                "dist": {
                    "url": "https://code.highcharts.com/8.2.2/modules/accessibility.js",
                    "type": "file"
                },
                "require": {
                    "composer/installers": "^1.0 || ^2.0"
                }
            }
        },
        {
            "type": "package",
                "package": {
                "name": "highcharts/3d",
                "version": "8.2.2",
                "type": "drupal-library",
                "extra": {
                    "installer-name": "highcharts_3d"
                },
                "dist": {
                    "url": "https://code.highcharts.com/8.2.2/highcharts-3d.js",
                    "type": "file"
                },
                "require": {
                    "composer/installers": "^1.0 || ^2.0"
                }
            }
        }
```

## Adding to site

run `composer require rohallion/localgov_elections_reporting`

## Block Placement

You will need to place the "Elections menu" block in the "Sidebar first" area of your LocalGov Drupal theme. It should be restricted to show on only the following pages:

```
/election/*
/election/*/*
/*/*/results
/*/*/share
/*/*/electoral-map
/*/*/candidates
```
